resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files-cnsq"
  acl           = "public-read"
  force_destroy = true # default is False (it doesn't allow you to destroy it without putting inputs) 
  # True is easily destroys bucket with terraform
}
