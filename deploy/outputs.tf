output "db_host" {
  value = aws_db_instance.main.address
}


## Add environment new EndPoints everytime you deploy 
## and that is the hostname that we can use to connect to 
## bastion from the public internet
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}


output "api_endpoint" {
  value = aws_lb.api.dns_name
}